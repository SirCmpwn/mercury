#include "constants.inc"
#include "platforms.inc"

; 0x0000
; RST 0x00
    jp z80_boot
; 0x0008
; RST 0x08
.block 0x08-$
    ret ; unused
.block 0x10-$
; 0x0010
; RST 0x10
    ret ; unused
.block 0x18-$
; 0x0018
; RST 0x18
    ret ; unused
.block 0x20-$
; 0x0020
; RST 0x20
    ret ; unused
.block 0x28-$
; 0x0028
; RST 0x28
    ret ; unused
.block 0x30-$
; 0x0030
; RST 0x30
    ret ; unused
.block 0x38-$
; 0x0038
; RST 0x38
; SYSTEM INTERRUPT
    ret ; unused
; 0x003B

.block 0x53-$
; 0x0053
    jp z80_boot
; 0x0056
;.db 0xFF, 0xA5, 0xFF

z80_boot:
    di
    ld a, 3 << MEM_TIMER_SPEED
    out (PORT_MEM_TIMER), a ; Memory mode 0

#ifdef FLASH4MB
    xor a
    out (PORT_MEMA_HIGH), a
    out (PORT_MEMB_HIGH), a
#endif

#ifdef CPU15
    ; Set memory mapping
    ; Bank 0: Flash Page 00
    ; Bank 1: Flash Page *
    ; Bank 2: RAM Page 01
    ; Bank 3: RAM Page 00 ; In this order for consistency with TI-83+ and TI-73 mapping
    ld a, 1 | BANKB_ISRAM_CPU5
    out (PORT_BANKB), a
#else
    ; Set memory mapping
    ; Bank 0: Flash Page 00
    ; Bank 1: Flash Page *
    ; Bank 2: RAM Page 01
    ; Bank 3: RAM Page 00
    ld a, 1 | BANKB_ISRAM_CPU6
    out (PORT_BANKB), a
#endif

    ld sp, 0

#ifdef CLOCK
    ld a, 1
    out (0x40), a
#endif

    ; Clear RAM
    ld hl, 0x8000
    ld (hl), 0
    ld de, 0x8001
    ld bc, 0x7FFF
    ldir

    ;call unlockFlash
    ;call unprotectRAM
    ;call unprotectFlash
    ;call lockFlash

#ifdef CPU15
    ld a, BIT_CPUSPEED_15MHZ
    out (PORT_CPUSPEED), a
#endif

    ld a, INT_ON | INT_TIMER1 | INT_LINK
    out (PORT_INT_MASK), a

    call initDisplay
    ld iy, 0x8000
    call clearBuffer
    ld de, 0
    ld hl, hello_world_text
    call drawStr
    call fastCopy
    jr $

    jp _kernel_main

hello_world_text:
    .asciiz "Hello world!"
